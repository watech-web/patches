WE Mega Menu has tons of issues that we're dealing with. We have a new patch that rolls in quite a few changes.
The patch, we-megamenu--3274934--3239043--3270040--hide-third-level.patch handles nearly every issue we've hit:
--hiding third level items
--config not saving
--saving a new page crashes
--deprecated code for PHP 8

Best to just use this patch for everything just in case.